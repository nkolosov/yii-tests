<?php
/**
 * @class MobileTest
 * Description of MobileTest class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */

Yii::import('yiivm.extensions.metadata.Metadata');

class ModuleTest extends CTestCase {
	public function testControllers() {
		$metadata = new Metadata;

		$modules = $metadata->getModules();

		$this->assertTrue(function_exists('curl_init'));

		foreach($modules as $module) {
			$this->assertTrue(Yii::app()->hasModule($module), Yii::t('test', 'Application haven\'t module {module}', array('{module}' => $module)));
			$moduleInstance = Yii::app()->getModule($module);

			$this->assertTrue($moduleInstance instanceof CModule);

			$controllersPath = $moduleInstance->getBasePath() . DIRECTORY_SEPARATOR . 'controllers';

			Yii::setPathOfAlias($module . '.controllers', $controllersPath);
			Yii::import($module . '.controllers.*');

			$controllers = $metadata->getControllers($module);

			foreach($controllers as $controller) {
				$syntaxCheck = exec('php -l ' . $controllersPath . '/' . $controller . '.php');
				$this->assertStringStartsWith('No syntax errors detected', $syntaxCheck);

				$controllerId = lcfirst(str_replace('Controller', '', $controller));
				/**
				 * @var CController $controllerInstance
				 */
				$controllerInstance = new $controller($controllerId, $module);
				$this->assertTrue($controllerInstance instanceof CController);

				$actions = $metadata->getActions($controller, $module);

				foreach($actions as $action) {
					$actionName = lcfirst($action);

					$ch = curl_init(Yii::app()->params->itemAt('baseUrl') . $module . '/' . $controllerId . '/' . $actionName);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_exec($ch);
					$result = (object) curl_getinfo($ch);
					//var_dump($result->http_code);
					//$this->
					//$this->assertFalse($result->http_code === 500, 'Method "' .$module . '/' . $controllerId . '/' . $actionName . '" return invalid status code');
				}
			}
		}
	}

	public function testModels() {
		Yii::setPathOfAlias('models', Yii::app()->basePath . '/models');
		Yii::import('models.*');

		$models = scandir(Yii::app()->basePath . '/models');

		foreach($models as $model) {
			if($model == '.' || $model == '..') {
				continue;
			}

			$modelName = str_replace('.php', '', $model);
			/**
			 * @var CActiveRecord $modelInstance
			 */
			$modelInstance = new $modelName;
			$this->assertTrue($modelInstance instanceof CActiveRecord);

			$attributes = array_keys($modelInstance->getAttributes());
			$safeAttributes = array_merge($modelInstance->getSafeAttributeNames(), array('id'));

			$this->assertTrue(count(array_unique(array_merge($attributes, $safeAttributes))) === count($attributes), 'DB or model hasn\'t fields: ' . implode(',', array_diff($attributes, $safeAttributes)));

		}
	}
}